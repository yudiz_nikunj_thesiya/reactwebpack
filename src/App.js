import React from "react";
import "./styles/app.scss";
import Home from "./pages/Home";

export const App = () => {
	return (
		<div className="app">
			<Home />
		</div>
	);
};
