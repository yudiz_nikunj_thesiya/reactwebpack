import axios from "axios";
import React, { useEffect, useState } from "react";
import ImageCard from "../components/Image";
import "../styles/home.scss";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";

const Home = () => {
	const [data, setData] = useState([]);
	useEffect(() => {
		axios
			.get(
				"https://api.unsplash.com/photos/?client_id=ISKDOtpzvl6lO8HpyRQYtqaS1jj_-8wdpRkelfnN01o&per_page=500&order_by=popular"
			)
			.then((response) => {
				setData(response?.data);
			});
	}, []);

	return (
		<div className="home">
			<ResponsiveMasonry
				columnsCountBreakPoints={{ 350: 2, 750: 2, 900: 3, 1000: 4 }}
			>
				<Masonry gutter={8}>
					{data?.map((img) => (
						<ImageCard data={img} key={img.id} />
					))}
				</Masonry>
			</ResponsiveMasonry>
		</div>
	);
};

export default Home;
