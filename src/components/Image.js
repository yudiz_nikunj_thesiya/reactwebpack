import React from "react";
import PropTypes from "prop-types";
import "../styles/imgcard.scss";

const ImageCard = ({ data }) => {
	return <img src={data?.urls?.small} alt="" />;
};

export default ImageCard;

ImageCard.propTypes = {
	data: PropTypes.object,
};
